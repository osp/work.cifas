# Autres fontes intéressantes

Voir http://luc.devroye.org/fonts-26839.html

* QTAgateType.otf
* QTAgateType-Bold.otf
* QTAgateType-Italic.otf

* QTBengal.otf
* QTBengal-Bold.otf

* QTKorrin.otf
* QTKorrin-Italic.otf

Et aussi https://www.suvatypefoundry.ee/tir-not-mono/#footer

* tir-notmono.otf
